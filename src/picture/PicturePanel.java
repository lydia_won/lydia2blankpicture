// This is the version that uses a loop to create rectangles for background

package picture;
/**
/**
 * PicturePanel.java
 * Author: Chuck Cusack
 * Date: August 22, 2007
 * Version: 2.0
 * 
 * Modified August 22, 2008
 *
 *An almost blank picture.
 *It just draws a few things.
 *
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * A class draws a picture on a panel
 *
 */
public class PicturePanel extends JPanel implements ActionListener, KeyListener{
	
	private boolean begin; // boolean to keep track of intro screen
	private boolean isPlaying; 
	int highlightButton = 0;
	int pressButton = 0;
	Timer t = new Timer(5, this);
	double x = 0, y = 0, velx = 0, vely = 0;	
    public PicturePanel() {  	
        MouseHandler mh=new MouseHandler();
        addMouseListener(mh);
        addMouseMotionListener(mh);
        
        begin = true;
        isPlaying = false; 
		t.start();
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);				
}

    /**
     * This method is called whenever the applet needs to be drawn.
     * This is the most important method of this class, since without
     * it, we don't see anything.
     * 
     * This is the method where you will most likely do all of your coding.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
                             
        if (begin) {
			createIntroScreen(g);
			drawStarthighlightButton(g);
			drawInstructionsButton(g);
			// writes my name in the bottom left corner
			g.setColor(Color.darkGray);
			g.setFont(new Font("Times Roman", Font.BOLD, 15));
			g.drawString("Lydia Won", getWidth() - 100, getHeight() - 20);
		} 
        if(pressButton == 2) {
        	instructionsTab(g);
        }
        if(pressButton == 3) {
        	begin = true; 
        }       
        if(isPlaying) {
        	Graphics2D g2 = (Graphics2D) g;   
        	g2.setColor(Color.GRAY);
	        g2.fill(new Ellipse2D.Double(x, y, 40, 40)); 
	        drawRestart(g);
	       
	        if(pressButton == 4) {
	        	createIntroScreen(g);
				drawStarthighlightButton(g);
				drawInstructionsButton(g);
				// writes my name in the bottom left corner
				g.setColor(Color.darkGray);
				g.setFont(new Font("Times Roman", Font.BOLD, 15));
				g.drawString("Lydia Won", getWidth() - 100, getHeight() - 20);
	        }    	   
        }
    }
    
    private void drawRestart(Graphics g) {   	
    	g.setColor(Color.gray);
        g.fill3DRect(710, 540, 60, 30, true);
        
        if(highlightButton == 4) {
        g.setColor(Color.CYAN);
        }
        else {
        	g.setColor(Color.GREEN);
        }
        g.drawString("restart", getWidth()-80, getHeight() - 20);

    }
    
    private void createIntroScreen(Graphics g) {
		super.paintComponent(g);
		
		g.setColor(Color.black);
		g.setFont(new Font("Times Roman", Font.BOLD, 27));
		g.drawString("This is an incredibly boring game", 160, 50);

		g.setFont(new Font("Brush Script", Font.ITALIC, 24));
		g.drawString("Press start to begin", 275, 90); 
		
		g.setFont(new Font("Copperplate Gothic Light", Font.ITALIC, 20));
		g.drawString("Press instructions for instructions...duh", 200, 130);
	}
    
    public void drawStarthighlightButton(Graphics g) {
		g.setColor(Color.red);
		g.fill3DRect(300, 260, 190, 75, true);
		g.setFont(new Font("Times Roman", Font.BOLD, 40));

		if (highlightButton == 1) {
			g.setColor(Color.white);
		} else {
			g.setColor(Color.black);
		}
		g.drawString("START", 330, 310);
	}
    
    public void drawInstructionsButton(Graphics g) {
		g.setColor(Color.red);
		g.fill3DRect(300, 360, 190, 75, true);
		g.setFont(new Font("Times Roman", Font.BOLD, 25));

		if (highlightButton == 2) {
			g.setColor(Color.white);
		} else {
			g.setColor(Color.black);
		}
		g.drawString("Instructions", 320, 405);
	}
    
    public void instructionsTab(Graphics g) {
    	g.setColor(Color.BLACK);
		g.fillRect(0, 0, 800, 600);
		g.setColor(Color.ORANGE);
		g.setFont(new Font ("American Typewriter", Font.ITALIC, 20));
		g.drawString("The goal of this game is to...", 235, 100);
		
		g.setColor(Color.LIGHT_GRAY); 	
		g.draw3DRect(getWidth() - 120, getHeight() - 90, 75, 50, false);
		
		g.setColor(Color.YELLOW);
		g.setFont(new Font ("Chalkboard", Font.BOLD, 16));
		g.drawString("back", getWidth() - 100, getHeight() - 60);
		
		g.setFont(new Font ("Chalkboard", Font.BOLD, 16));

		if(highlightButton == 3) {
			g.setColor(Color.YELLOW);
		}
		else {
			g.setColor(Color.BLUE);
		}   
			g.drawString("back", getWidth() - 100, getHeight() - 60);					
    }   
      
    //------------------------------------------------------------------------------------------
    
     //---------------------------------------------------------------
    // A class to handle the mouse events for the applet.
    // This is one of several ways of handling mouse events.
    // If you do not want/need to handle mouse events, delete the following code.
    //
    private class MouseHandler extends MouseAdapter implements MouseMotionListener
    {
      
        public void mouseClicked(MouseEvent e) {          
        	repaint();
        }
        public void mouseEntered(MouseEvent e) {
        }
        public void mouseExited(MouseEvent e) {
        }        
        public void mousePressed(MouseEvent e) {
        	 int x = e.getX();
             int y = e.getY();
             if (begin) {
             	if (x>300 && x<490) {
             		if (y>260 && y<335) {
             			pressButton = 1;
             			begin = false; 
             			isPlaying = true; 
             		}
             		else if (y > 360 && y < 435) {
             			pressButton = 2; 
             			begin = false;            			
             	}
             	}
             }
             if(!begin && !isPlaying) {
            	 if (x > getWidth()-120 && x < (getWidth()-100)+75) {
     				if ( y > getHeight()-90 && y<(getWidth()-60)+50) {
     					pressButton = 3; 
     					begin = true; 
     				}
            	 }
             }
             if(isPlaying && !begin) {
            	 if (x > 710 && x < 800) {
            		 if (y > 540 && y > 600) {
            			 pressButton = 4; 
            			 isPlaying = false; 
         	        	 begin = true; 
            		 }           			 
            	 }
             }
             repaint();            
        }        
        public void mouseReleased(MouseEvent e) {
        }
        public void mouseMoved(MouseEvent e) {  	
        	//FIX THIS PART!!
        	int x = e.getX();
			int y = e.getX();
			if(begin) {
			if (x > 300 && x < 490) {
				if (y > 260 && y < 335) {
					highlightButton = 1;
				}
				else if (y > 360 && y < 435) {
					highlightButton = 2;
				}
			} 
			else {
				if (highlightButton != 0) {
					highlightButton = 0;
				}	
			}
			}
			if(!begin && !isPlaying) {
           	 	if (x > getWidth()-120 && x < (getWidth()-100)+75) {
    				if ( y > getHeight()-90 && y<(getWidth()-60)+50) {
    					highlightButton = 3; 
    					begin = true; 
    				}          	 
           	 	}					
			}
			if(isPlaying && !begin) {
           	 	if (x > 710 && x < 800) {
           	 		if (y > 540 && y > 600) {
           	 			highlightButton = 4;           			 
           	 		}       			 
           	 	}
            }
		repaint(); 
        }
        
        public void mouseDragged(MouseEvent e) {
        }
    }
    // End of MouseHandler class

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int code = e.getKeyCode();
		if (code == KeyEvent.VK_UP) {
			up();
		}
		if (code == KeyEvent.VK_DOWN) {
			down();
		}
		if (code == KeyEvent.VK_LEFT) {
			left();
		}
		if (code == KeyEvent.VK_RIGHT) {
			right();
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
		if (x < 0 || x > 760) {
			velx = -velx;
		}
		if (y < 0 || y > 535) {
			vely = -vely;
		}
		x += velx;
		y += vely;
	}
	public void up() {
		vely += -2.0;
		velx = 0;
	}
	public void down() {
		vely += 2.0;
		velx = 0;
	}
	public void left() {
		vely = 0;
		velx += -2.0;
	}
	public void right() {
		vely = 0;
		velx += 2.0;
	}
    
} // Keep this line--it is the end of the PicturePanel class